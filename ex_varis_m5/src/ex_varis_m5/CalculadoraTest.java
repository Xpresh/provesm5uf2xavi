package ex_varis_m5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CalculadoraTest {

	@Test
	void testSuma() {
		int res = Calculadora.suma(10,20);
		assertEquals(30, res);
		//fail("Not yet implemented");
	}

	@Test
	void testResta() {
		int res = Calculadora.resta(20,10);
		assertEquals(10, res);
		//fail("Not yet implemented");
	}

	@Test
	void testMultiplicacio() {
		int res = Calculadora.multiplicacio(10,20);
		assertEquals(200, res);
		//fail("Not yet implemented");");
	}

	@Test
	void testDivisio() {
		int res = Calculadora.divisio(20,10);
		assertEquals(2, res);
		//fail("Not yet implemented");
	}
	
	/*@Test
	void testDivideix() {
		int res = Calculadora.divideix(12,0);
	}*/
	
	/*@Test
	void testException() {
		int res;
		try {
			res = Calculadora.divideix(12,0);
			fail("Fallo: Passa per aqu� si no es llan�a l'excepci� ArithmeticException");
		} catch (ArithmeticException e) {
			//La prova funciona correctament
		}
	}*/
	
	@Test
	void testExpectedException() {
		Assertions.assertThrows(ArithmeticException.class,  () -> {
			Calculadora.divideix(10,0);
		});
	}

}
